////////////////////////////////////////////////////////////////////////////////
//  ARC_Bridge.h
//  UnitTestsPresentation
//
//  Created by abelenkov on 9/27/15.
//  Copyright © 2015 abelenkov. All rights reserved.
/////////////////////////////////////////////////////////////////////////////////

//! @file ARC_Bridge.h
//! @defgroup Default
//! @ingroup Default
//@{

#if !defined(TPSuppressPerformSelectorLeakWarning)

#define TPSuppressPerformSelectorLeakWarning(Stuff) \
	do \
	{ \
		_Pragma("clang diagnostic push") \
		_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
		Stuff; \
		_Pragma("clang diagnostic pop") \
	} while (0)

#define TPSuppressCaptureStronglyLeakWarning(Stuff) \
	do \
	{ \
		_Pragma("clang diagnostic push") \
		_Pragma("clang diagnostic ignored \"-Warc-retain-cycles\"") \
		Stuff; \
		_Pragma("clang diagnostic pop") \
	} while (0)

#endif

//@}
