//
//  URLSessionStub.m
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

#import "URLSessionStub.h"
#import "OCMock.h"
#import <UIKit/UIKit.h>

@interface URLSessionStub()

@property (nonatomic, strong) id URLSessionClassMock;

@end

@implementation URLSessionStub

- (void)startMocking {
	self.URLSessionClassMock = OCMClassMock([NSURLSession class]);
	[[[self.URLSessionClassMock stub] andReturn:[self sharedSessionMock]] sharedSession];
	
}

- (void)stopMocking {
	[self.URLSessionClassMock stopMocking];
	self.URLSessionClassMock = nil;
}

#pragma mark - Stub
- (id)sharedSessionMock {
	return self;
}

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request completionHandler:(void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error))completionHandler {
	UIImage *theImage = [UIImage imageNamed:@"images"];
    NSError *theError = [[NSError alloc] initWithDomain:@"testDomain" code:42 userInfo:nil];
	completionHandler(nil, nil, theError);
	return nil;
}

@end
