//
//  TestUsersSearchDataSource.swift
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class TestUsersSearchDataSource: UsersSearchDataSource {
	var totalCount: UInt = 120
	var lastPage: UInt = 1
	
	var _users: [UserModel] = []
	override var users: [UserModel] {
		return _users
	}
	
	override internal func beginSearch(userName: String, completion: UserSearchCompletion) {
		lastPage = 1
		_users = []
		for i in 1...30 {
			let user = UserModel.init(userName: "test\(i)", avatarURL: URL(string:"http://testURL.com")!)
			_users.append(user)
		}
		completion?(nil)
	}
	
	override internal func loadNextPageIfNeeded(indexPath:IndexPath, completion: UserSearchCompletion) {
		if indexPath.row >= users.count - 1 && totalCount > users.count {
			lastPage += 1
			let count = users.count - 1
			for i in count...count + 29 {
				let user = UserModel.init(userName: "test\(i)", avatarURL: URL(string:"http://testURL.com")!)
				_users.append(user)
			}
			completion?(nil)
		}
	}
	
	override internal func loadFullInfo(index: Int, completion: UserSearchCompletion) {
		_users[index].followers = 42
		_users[index].publicRepos = 4242
		_users[index].avatarImage = UIImage(named: "images")
		completion?(nil)
	}
}
