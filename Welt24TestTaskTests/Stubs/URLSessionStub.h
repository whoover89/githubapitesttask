//
//  URLSessionStub.h
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLSessionStub : NSObject

- (void)startMocking;
- (void)stopMocking;

@end
