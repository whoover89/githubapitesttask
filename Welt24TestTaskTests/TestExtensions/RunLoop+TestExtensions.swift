//
//  RunLoop+TestExtensions.swift
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

extension RunLoop {
	public func runForTime(aTimeInterval:TimeInterval) {
		let theInitialDate = Date()
		while self.run(mode:.defaultRunLoopMode, before:Date.init(timeIntervalSinceNow:min(1.0, aTimeInterval))) {
			if Date().timeIntervalSince(theInitialDate) > aTimeInterval {
				break
			}
		}
	}
	
	@discardableResult public func runForTime(aTimeInterval:TimeInterval, aTargetView:UIView, aTargetImageName:String) -> Bool {
		let theInitialDate = Date()
		let theImagePath = aTargetView.gtm_path(forImageNamed: aTargetImageName)
		while self.run(mode:.defaultRunLoopMode, before:Date.init(timeIntervalSinceNow:min(0.25, aTimeInterval))) {
			if Date().timeIntervalSince(theInitialDate) > aTimeInterval {
				break
			}
		}
		return autoreleasepool { () -> Bool in
			if (nil != theImagePath && aTargetView.gtm_compareWithImage(at:theImagePath, diffImage:nil)) {
				return true
			}
			return false
		}
	}
}
