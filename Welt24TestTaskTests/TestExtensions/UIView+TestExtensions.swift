//
//  UIView+TestExtensions.swift
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

extension UIView {
	internal func findView<T>(of type:T.Type, identifier:String) -> T? where T:UIView {
		return findView(of:type, key:"identifier", value:identifier)
	}
	
	internal func findView<T, U>(of type:T.Type, key:String, value:U?) -> T? where T:UIView, U:Equatable {
		let theViews = findAllViews(of:type)
		var theNeededView : T?
		for theView in theViews {
			if let theViewValue = theView.value(forKey:key) as? U {
				if theViewValue == value {
					theNeededView = theView
					break
				}
			}
		}
		return theNeededView
	}
	
	internal func findView<T>(of type:T.Type) -> T? where T:UIView {
		var theFoundView : T?
		
		if self.isKind(of:type) {
			theFoundView = self as? T
		} else {
			for theView in self.subviews {
				theFoundView = theView.findView(of:type)
				if nil != theFoundView {
					return theFoundView
				}
			}
		}
		
		return theFoundView
	}
	
	internal func findAllViews<T>(of type:T.Type) -> [T] where T:UIView {
		var theViews : [T] = []
		findAllViews(of:type, views: &theViews)
		return theViews
	}
	
	private func findAllViews<T>(of type:T.Type, views: inout [T]) where T:UIView {
		if self.isKind(of:type) && !views.contains(self as! T) {
			views.append(self as! T)
		}
		
		for theView in self.subviews {
			if theView.isKind(of:type) && !views.contains(theView as! T) {
				views.append(theView as! T)
			}
			theView.findAllViews(of:type, views: &views)
		}
	}
}
