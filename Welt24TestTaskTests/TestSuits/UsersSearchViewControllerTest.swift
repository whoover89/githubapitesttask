//
//  UsersSearchViewControllerTest.swift
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import XCTest

class UsersSearchViewControllerTest: BaseTestCase {
	func testController() {
		let storyboard = UIStoryboard(name: "Main", bundle: Bundle.init(for: type(of: self)))
		XCTAssertNotNil(storyboard)
		
		let controller = storyboard.instantiateViewController(withIdentifier: "UsersSearchViewController") as? UsersSearchViewController
		XCTAssertNotNil(controller)
		rootViewController = UINavigationController(rootViewController:controller!)
		compareRootViewController(aName: "UsersSearchViewController-Initial")
		
		var theSearchBar : UISearchBar!
		if let searchBar = rootViewController?.view.findAllViews(of: UISearchBar.self).first {
			searchBar.text = "a"
			searchBar.delegate?.searchBar!(searchBar, textDidChange: "a")
			compareRootViewController(aName: "UsersSearchViewController-1stSearch")
			theSearchBar = searchBar
		} else {
			XCTAssertTrue(false, "search bar not found")
		}
		var theTableView : UITableView!
		if let tableView = rootViewController?.view.findAllViews(of: UITableView.self).first {
			theTableView = tableView
			XCTAssertEqual(30, tableView.dataSource?.tableView(tableView, numberOfRowsInSection: 0))
			compareRootViewController(aName: "UsersSearchViewController-Scrolled30")
			tableView.scrollToRow(at: IndexPath.init(row: 29, section: 0), at: .bottom, animated: true)
			XCTAssertEqual(60, tableView.dataSource?.tableView(tableView, numberOfRowsInSection: 0))
			compareRootViewController(aName: "UsersSearchViewController-Scrolled60")
		} else {
			XCTAssertTrue(false, "table view not found")
		}
		theSearchBar.delegate?.searchBar!(theSearchBar, textDidChange: "b")
		XCTAssertEqual(30, theTableView.dataSource?.tableView(theTableView, numberOfRowsInSection: 0))
		
		tester().tapRow(at: IndexPath.init(row: 2, section: 0), in: theTableView)
		compareRootViewController(aName: "UserDetailsViewController-Initial")
	}
}
