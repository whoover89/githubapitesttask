//
//  UsersSearchViewControllerTest.swift
//  Welt24TestTaskTests
//
//  Created by abelenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import XCTest
import UIKit

class DataConvertableRequestTest: BaseTestCase {
    private let session = URLSession.shared
    
    func testRequest() {
        guard let testURL = URL.init(string: "http://placehold.it/120x120&text=image1") else {
            XCTAssertTrue(false)
            return
        }
        
        let testExpectation = expectation(description: "Wait for request finishing")
        let request = DataConvertableRequest<UIImage>.makeRequest(session: session, url: testURL, completion:
        { (outImage, outError) in
            XCTAssertNotNil(outImage)
            XCTAssertNil(outError)
            testExpectation.fulfill()
        }) { (outIdentifier) in
            XCTAssertNotNil(outIdentifier)
        }
        XCTAssertNotNil(request)
        waitForExpectations(timeout: 10.0)
        { (outError) in
            XCTAssertNil(outError)
        }
    }
	
	func testWithMockedSession() {
		let urlSessionStub = URLSessionStub()
		urlSessionStub.startMocking()
		
		guard let testURL = URL.init(string: "http://placehold.it/120x120&text=image1") else {
			XCTAssertTrue(false)
			return
		}
		
		let testExpectation = expectation(description: "Wait for request finishing")
		let request = DataConvertableRequest<UIImage>.makeRequest(session: URLSession.shared, url: testURL, completion:
		{ (outImage, outError) in
			XCTAssertNil(outImage)
			XCTAssertNotNil(outError)
			testExpectation.fulfill()
		}) { (outIdentifier) in
			XCTAssertNil(outIdentifier)
		}
		XCTAssertNotNil(request)
		waitForExpectations(timeout: 10.0)
		{ (outError) in
			XCTAssertNil(outError)
		}
		
		urlSessionStub.stopMocking()
	}
}
