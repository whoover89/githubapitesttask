//
//  BaseTestCase.swift
//  Welt24TestTaskTests
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import XCTest
import UIKit

class BaseTestCase: KIFTestCase {
	var shouldResetSuperView: Bool = false
	
	var keyWindow: UIWindow? {
		return UIApplication.shared.keyWindow
	}
	
	var rootViewController: UIViewController? {
		get {
			return keyWindow?.rootViewController
		}
		set {
			keyWindow?.rootViewController = newValue
			keyWindow?.makeKeyAndVisible()
			RunLoop.current.run(until: Date().addingTimeInterval(0.1))
		}
	}
	
	private var _viewToTest: UIView?
	var viewToTest: UIView? {
		get {
			return _viewToTest
		}
		set {
			if shouldResetSuperView {
				keyWindow?.backgroundColor = UIColor.white
				_viewToTest?.removeFromSuperview()
				self.shouldResetSuperView = false
			}
			_viewToTest = newValue
			if _viewToTest != nil && viewToTest?.superview != nil {
				shouldResetSuperView = true
				keyWindow?.backgroundColor = UIColor.black
				keyWindow?.addSubview(_viewToTest!)
			}
		}
	}
	
	override func beforeEach() {
		super.beforeEach()
		
		keyWindow?.backgroundColor = UIColor.white
		rootViewController = UIViewController()
		continueAfterFailure = true
	}
	
	override func afterEach() {
		viewToTest = nil
		RunLoop.current.run(until: Date().addingTimeInterval(0.1))
		
		super.afterEach()
	}
}

extension BaseTestCase {
	
	internal func compareTestView(aName:String, _ aDescription:String? = nil, _ function: String = #function,
								  _ line: Int = #line)
	{
		compare(viewToTest, toImageNamed:aName, delay: 5.0, aDescription, function, line)
	}
	
	internal func compareRootViewController(aName: String, _ aDescription:String? = nil, _ function: String = #function,
											_ line: Int = #line)
	{
		compare(nil, toImageNamed:aName, delay: 5.0, aDescription, function, line)
	}
	
	internal func compareWindowContentView(aName:String, _ aDescription:String? = nil, _ function: String = #function,
										   _ line: Int = #line)
	{
		compare(keyWindow, toImageNamed:aName, delay: 5.0, aDescription, function, line)
	}
	
	internal func compare(_ aView: UIView?, toImageNamed anImageName: String, delay:Double = 5.0,
						  _ aDescription: String? = nil, _ function: String = #function, _ line: Int = #line)
	{
		var theViewToAssert : UIView? = aView
		if (nil == theViewToAssert)
		{
			theViewToAssert = keyWindow?.rootViewController?.view
		}
		let theDelay = max(delay, 0.0)
		RunLoop.current.runForTime(aTimeInterval:theDelay, aTargetView:theViewToAssert!, aTargetImageName:anImageName)
		
		compareObjectImage(object:theViewToAssert!, to:anImageName, description:aDescription, function, line)
	}
	
	private func compareObjectImage(object:Any, to imageNamed:String, description:String?, _ function: String, _ line: Int)
	{
		var theErrorString : NSString?
		if (!GTMIsObjectImageEqualToImageNamed(object, imageNamed, &theErrorString)) {
			let theFailedString =
				"\n\n" +
					"----- Failed image comparing: -----\n" +
					"class:\(type(of:self))\n" +
					"function:\(function)\n" +
					"line:\(line)\n" +
					"imageName:\(imageNamed)\n" +
					"-----------------------------------" +
			"\n\n"
			XCTFail(theFailedString)
		}
	}
	
}

extension XCTestCase {
	func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor {
		return KIFUITestActor(inFile: file, atLine: line, delegate: self)
	}
	
	func system(file : String = #file, _ line : Int = #line) -> KIFSystemTestActor {
		return KIFSystemTestActor(inFile: file, atLine: line, delegate: self)
	}
}

extension KIFTestActor {
	func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor {
		return KIFUITestActor(inFile: file, atLine: line, delegate: self)
	}
	
	func system(file : String = #file, _ line : Int = #line) -> KIFSystemTestActor {
		return KIFSystemTestActor(inFile: file, atLine: line, delegate: self)
	}
}
