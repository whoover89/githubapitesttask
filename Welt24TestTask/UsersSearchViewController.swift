//
//  ViewController.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/18/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class UsersSearchViewController: UITableViewController {
	
	@IBOutlet private weak var searchBar: UISearchBar!
	private var dataSource : UsersSearchDataSource!
	private let cellIdentifier = "userCellIdentifier"
	private var alertController : UIAlertController?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		dataSource = UsersSearchDataSource.createDataSource(reloadHandler:
		{ (outIndex) in
			self.tableView.reloadCell(index: outIndex, section: 0)
		})
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataSource.users.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
		let model = dataSource.users[indexPath.row]
		cell.textLabel?.text = model.userName
		cell.imageView?.image = model.avatarImage
		dataSource.loadNextPageIfNeeded(indexPath: indexPath)
		{ (outError) in
			if let error = outError {
				self.showAlert(text: error.localizedDescription)
			}
			self.tableView.reloadData()
		}
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if let detailedViewController = storyboard?.instantiateViewController(withIdentifier: "UserDetailsViewController") as? UserDetailsViewController {
			dataSource.loadFullInfo(index: indexPath.row, completion:
			{ (outError) in
				if let error = outError {
					self.showAlert(text: error.localizedDescription)
				} else {
					let model = self.dataSource.users[indexPath.row]
					detailedViewController.userModel = model
					self.navigationController?.pushViewController(detailedViewController, animated: true)
				}
			})
		}
	}
	
	private func showAlert(text: String) {
		if alertController == nil {
			let alertActionOK = UIAlertAction.init(title: "OK", style: .default, handler:
			{ (outAction) in
				self.alertController = nil
			})
			let alertActionTryAgain = UIAlertAction.init(title: "Try Again", style: .default, handler:
			{ (outAction) in
				self.beginSearch(text: self.searchBar.text ?? "")
				self.alertController = nil
			})
			alertController = UIAlertController.init(title: "Error", message: text, preferredStyle: .alert)
			alertController!.addAction(alertActionOK)
			alertController!.addAction(alertActionTryAgain)
			present(alertController!, animated: true, completion: nil)
		}
	}
	
	fileprivate func beginSearch(text: String) {
		dataSource.beginSearch(userName: text)
		{ (outError) in
			if let error = outError {
				self.showAlert(text: error.localizedDescription)
			}
			self.tableView.reloadData()
		}
	}
}

extension UsersSearchViewController : UISearchBarDelegate {
	internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		beginSearch(text: searchBar.text ?? "")
	}
}
