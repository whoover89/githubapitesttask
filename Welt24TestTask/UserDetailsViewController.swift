//
//  UserDetailsViewController.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {
	@IBOutlet private var userNameLabel: UILabel!
	@IBOutlet private var publicReposLabel: UILabel!
	@IBOutlet private var followersLabel: UILabel!
	@IBOutlet private var avatarImageView: UIImageView!
	internal var userModel: UserModel?
	
    override func viewDidLoad() {
        super.viewDidLoad()

        userNameLabel.text = userModel?.userName
		publicReposLabel.text = String(userModel?.publicRepos ?? 0)
		followersLabel.text = String(userModel?.followers ?? 0)
		avatarImageView.image = userModel?.avatarImage
    }
}
