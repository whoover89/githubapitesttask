//
//  CancallableTasksHandler.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class CancallableTasksHandler {
	private let requestsSycnQueue = DispatchQueue(label: "com.W24TT.SessionWrapper.taskSycnQueue")
	private var activeRequests : [String:ICancelable] = [:]
	
	func cancelRequest(identifier: String) {
		requestsSycnQueue.sync {
			activeRequests[identifier]?.cancel()
			activeRequests[identifier] = nil
		}
	}
	
	func cancelAll() {
		requestsSycnQueue.sync {
			for (_, request) in activeRequests {
				request.cancel()
			}
			activeRequests.removeAll()
		}
	}
	
	func add(request: ICancelable?) {
		if let theRequest = request, let identifier = theRequest.identifier {
			requestsSycnQueue.sync {
				activeRequests[identifier] = theRequest
			}
		}
	}
	
	func removeRequest(identifier: String?) {
		if let theIdentifier = identifier {
			requestsSycnQueue.sync {
				activeRequests[theIdentifier] = nil
			}
		}
	}
}
