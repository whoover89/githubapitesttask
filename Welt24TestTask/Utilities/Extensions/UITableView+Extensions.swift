//
//  UITableView+Extensions.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

extension UITableView {
	func reloadCell(index: Int, section: Int) {
		self.beginUpdates()
		self.reloadRows(at:[IndexPath(row: index, section: section)], with: .none)
		self.endUpdates()
	}
}
