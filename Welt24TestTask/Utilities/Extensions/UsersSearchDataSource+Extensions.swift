//
//  UserSearchDataSource.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/20/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

extension UsersSearchDataSource {
	static func createDataSource(reloadHandler: ReloadHandler) -> UsersSearchDataSource {
		return UsersSearchDataSource(reloadHandler: reloadHandler)
	}
}
