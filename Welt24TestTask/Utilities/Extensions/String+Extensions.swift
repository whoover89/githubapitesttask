//
//  String+Extensions.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import Foundation

extension String {
    func adding(requestParameters:[String:String], queryParameters:[[String:String]]) -> String {
        var string = self
        
        if requestParameters.count > 0 {
            string += "?" + requestParameters.flatMap({ (inValues) -> String? in
                return inValues.key + "=" + inValues.value
            }).joined(separator: "&")
        }
        
        if queryParameters.count > 0 {
            string += "&q=" + queryParameters.map(
            {
                return $0.flatMap( {$0.key + ($0.value.count > 0 ? (":" + $0.value) : "")})
            }).joined(separator: "+")
        }
        
        return string
    }
}
