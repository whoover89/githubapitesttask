//
//  UsersSearchDataSource.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit
import Carlos

typealias UserSearchCompletion = ((_ error: Error?) -> Void)?
typealias ReloadHandler = ((_ index: Int) -> Void)?
class UsersSearchDataSource {
	private(set) internal var users: [UserModel] = []
	private let networkManager: SessionWrapper = SessionWrapper()
	private let perPage: UInt = 30
	private var lastPage: UInt = 1
	private var totalCount: UInt = 0
	private var lastSearchString: String = ""
	private let cache = MemoryCacheLevel<String, UIImage>().compose(DiskCacheLevel())
	private var reloadHandler: ReloadHandler
	
	init(reloadHandler: ReloadHandler) {
		self.reloadHandler = reloadHandler
	}
	
	internal func beginSearch(userName: String, completion: UserSearchCompletion) {
		lastPage = 1
		networkManager.cancelAll()
		users = []
		lastSearchString = userName
		search(userName: userName, completion: completion)
	}
	
	internal func loadNextPageIfNeeded(indexPath:IndexPath, completion: UserSearchCompletion) {
		if indexPath.row >= users.count - 1 && totalCount > users.count {
			lastPage += 1
			search(userName: lastSearchString, completion: completion)
		}
	}
	
	internal func loadFullInfo(index: Int, completion: UserSearchCompletion) {
		let user = users[index]
		networkManager.getAdditionalInfo(userName: user.userName)
		{ (outUserInfo, outError) in
			DispatchQueue.main.async {
				user.followers = outUserInfo?.followers
				user.publicRepos = outUserInfo?.publicRepos
				completion?(outError)
			}
		}
	}
	
	private func search(userName: String, completion: UserSearchCompletion) {
		networkManager.search(userName: userName, page: lastPage, perPage: perPage)
		{ (outUsersResponse, outError) in
			var error = outError
			DispatchQueue.main.async {
				if error == nil {
					if let theResponse = outUsersResponse {
						if let theErrorMessage = theResponse.errorMessage {
							error = NSError(domain: "gitgub", code: 707, userInfo: [NSLocalizedDescriptionKey: theErrorMessage])
						} else if let theUsers = theResponse.users {
							self.add(usersResponse: theUsers)
							self.totalCount = theResponse.totalCount ?? 0
						}
					}
				}
				
				completion?(error)
			}
		}
	}
	
	private func add(usersResponse:[UsersSearchResponse.UserSearchModel]) {
		for userResponse in usersResponse {
			if let theURL = URL(string:userResponse.avatarURLString) {
				let userModel = UserModel(userName: userResponse.userName,
					avatarURL:theURL)
				let index = self.users.count
				loadImage(url: theURL, completion: { (outImage) in
					DispatchQueue.main.async {
						userModel.avatarImage = outImage
						self.reloadHandler?(index)
					}
				})
				self.users.append(userModel)
			}
		}
	}
	
	private func loadImage(url:URL, completion:((_ image: UIImage) -> Void)?) {
		cache.get(url.absoluteString).onSuccess(
		{ (outImage) in
			completion?(outImage)
		}).onFailure({ (outError) in
			self.networkManager.getImage(urlString: url.absoluteString, completion:
			{[weak self] (outImage, outError) in
				if let image = outImage {
					completion?(image)
					_ = self?.cache.set(image, forKey: url.absoluteString)
				}
			})
		})
	}
}
