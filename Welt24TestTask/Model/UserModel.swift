//
//  UserModel.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

typealias UserAvatarRequestHandler = (() -> Void)?
class UserModel {
    private(set) var avatarURL: URL?
    private(set) var userName: String
    internal var followers: UInt?
    internal var publicRepos: UInt?
    internal var avatarImage: UIImage?
    
	init(userName: String, avatarURL: URL) {
        self.userName  = userName
        self.avatarURL = avatarURL
    }
}
