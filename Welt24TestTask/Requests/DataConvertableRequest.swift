//
//  DataConvertableRequest.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class DataConvertableRequest<T:IDataConvertable>: RawRequest<T> {
    override func serializeToResponse(data: Data, anError: inout Error?) -> T? {
        return T.init(data:data)
    }
}
