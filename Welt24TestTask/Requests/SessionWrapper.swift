//
//  NetworkingManager.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/18/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class SessionWrapper {
	private let githubBaseURL = "https://api.github.com"
	private let searchUsersEndPoint = "/search/users"
	private let userPageEndPoint = "/users/"
    
    private let session : URLSession
	private let requestsHandler = CancallableTasksHandler()
    
    deinit {
        requestsHandler.cancelAll()
    }
    
    init() {
        session = URLSession()
    }
	
	internal func cancelAll() {
		requestsHandler.cancelAll()
	}
	
	internal func search(userName: String, page: UInt, perPage: UInt = 100, completion:RawRequest<UsersSearchResponse>.BaseRequestCompletion) {
        let requestParameters : [String:String] = [
            "page" : String(page),
            "per_page" : String(perPage)
        ]
        let queryParameters : [[String:String]] = [[userName : ""], ["type" : "user"]]

        let urlString = (githubBaseURL + searchUsersEndPoint).adding(requestParameters: requestParameters, queryParameters: queryParameters)
        
        guard let url = URL(string: urlString) else {
            return
        }
        make(requestType: JSONRequest<UsersSearchResponse>.self, url: url, completion: completion)
	}
	
	internal func getAdditionalInfo(userName: String, completion: RawRequest<UserInfoResponse>.BaseRequestCompletion) {
		let urlString = githubBaseURL + userPageEndPoint + userName
        guard let url = URL(string: urlString) else {
            return
        }
        make(requestType: JSONRequest<UserInfoResponse>.self, url: url, completion: completion)
	}
	
	internal func getImage(urlString: String, completion: RawRequest<UIImage>.BaseRequestCompletion) {
        guard let url = URL(string: urlString) else {
            return
        }
        make(requestType: DataConvertableRequest<UIImage>.self, url: url, completion: completion)
	}
    
    private func make<T:Any>(requestType:RawRequest<T>.Type, url:URL , completion: RawRequest<T>.BaseRequestCompletion) {
		let request = requestType.makeRequest(session: session, url: url, completion: completion, finished:
		{[weak self] (outIdentifier) in
			self?.requestsHandler.removeRequest(identifier: outIdentifier)
		})
        requestsHandler.add(request: request)
    }
}
