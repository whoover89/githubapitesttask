//
//  ICancellable.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import Foundation

protocol ICancelable {
    var identifier: String? {get}
	
    func cancel()
}

extension URLSessionTask: ICancelable {
    var identifier: String? {
        return String(taskIdentifier)
    }
}
