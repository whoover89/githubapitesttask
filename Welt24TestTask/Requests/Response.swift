//
//  UserModel.swift
//  Welt24TestTask
//
//  Created by Artyom Belenkov on 2/18/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import Foundation

struct UsersSearchResponse : Codable {
    struct UserSearchModel : Codable {
        let avatarURLString: String
        let userName: String
        
        enum CodingKeys: String, CodingKey {
            case avatarURLString = "avatar_url"
            case userName = "login"
        }
    }
    
    let users : [UserSearchModel]?
	let totalCount : UInt?
	let errorMessage: String?
	
	enum CodingKeys: String, CodingKey {
		case users = "items"
		case errorMessage = "message"
		case totalCount = "total_count"
	}
}

struct UserInfoResponse : Codable {
	let followers: UInt
	let publicRepos: UInt
	
	enum CodingKeys: String, CodingKey {
		case followers
		case publicRepos = "public_repos"
	}
}
