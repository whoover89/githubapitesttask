//
//  IDataConvertable.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

protocol IDataConvertable {
    init?(data:Data)
}

extension UIImage: IDataConvertable {}
extension Data: IDataConvertable {
    init?(data: Data) {
        self = data
    }
}
