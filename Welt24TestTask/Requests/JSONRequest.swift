//
//  JSONRequest.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import UIKit

class JSONRequest<T:Codable>: RawRequest<T> {
    private let decoder: JSONDecoder
    
	required init(url: URL, completion:BaseRequestCompletion, finished:RequestFinishedHandler = nil) {
        self.decoder = JSONDecoder()
		super.init(url: url, completion: completion, finished: finished)
    }
    
    override func serializeToResponse(data: Data, anError: inout Error?) -> T? {
        var theResponse : T?
        do {
            theResponse = try decode(data: data, to: T.self)
        } catch {
            anError = error
        }
        return theResponse
    }
    
    func decode(data: Data, to type:T.Type) throws -> T? {
        return try self.decoder.decode(type, from: data)
    }
}
