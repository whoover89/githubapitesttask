//
//  BaseRequest.swift
//  Welt24TestTask
//
//  Created by abelenkov on 2/19/18.
//  Copyright © 2018 Artyom Belenkov. All rights reserved.
//

import Foundation

class RawRequest<T:Any> {
	typealias BaseRequestCompletion = ((_ response: T?, _ error: Error?) -> Void)?
	typealias RequestFinishedHandler = ((_ identifier: String?) -> Void)?
	
    private var url: URL
    private var completion: BaseRequestCompletion
	private var finishedHandler: RequestFinishedHandler
	private var task: URLSessionTask?
	
	class func makeRequest(session:URLSession, url: URL, completion: BaseRequestCompletion, finished: RequestFinishedHandler = nil) -> ICancelable {
		let request = self.init(url: url, completion: completion, finished: finished)
		request.start(session: session)
		return request
	}
	
	required init(url: URL, completion: BaseRequestCompletion, finished: RequestFinishedHandler = nil) {
        self.url = url
        self.completion = completion
		self.finishedHandler = finished
    }
	
	internal func start(session: URLSession) {
		let request = URLRequest(url: url)
		task = URLSession.shared.dataTask(with: request, completionHandler:
		{[weak self] (outData, outResponse, outError) in
			var theError = outError
			var theResponse : T?
			if theError == nil, let data = outData {
				theResponse = self?.serializeToResponse(data: data, anError: &theError)
			}
			self?.completion?(theResponse, theError)
			self?.finishedHandler?(self?.identifier)
		})
		task?.resume()
	}
    
    internal func serializeToResponse(data: Data, anError: inout Error?) -> T? {
        fatalError("Should be overrided")
    }
}

extension RawRequest : ICancelable {
	var identifier: String? {
		return task?.identifier
	}
	
	func cancel() {
		task?.cancel()
	}
}
